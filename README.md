# ![Spgeed](docs/assets/images/logo-big.svg)

Spgeed (pronounce `speed`) stands for "System to PostGresql Extended Exploitation of Database".

Spgeed is a lightweight JAVA library that gives you the opportunity to use the full potential of [PostgreSQL](https://www.postgresql.org) database with the flexibility of an ORM and the efficiency of direct SQL calls.

You need at least Postgresql version 9.5, Java version 8 and enable the debug parameter in javacc.

You can find more information on the [website](http://spgeed.org/);

## Create your first request

With Spgeed, requests are always written in native SQL. To execute a request, you need to create a DAO interface. Each method of that interface should define a sql statement as an annotation. The method parameters can be used in the statement using this simple "${parameter}" expression. The result of the statement will be mapped with the method return type.

``` java
public interface JourneyDao {

    @Update(sql = "INSERT INTO journey (name, location) " +
                "VALUES (${name}, ${location})")
    int save(String name, String location);

    @Select(sql = "SELECT * FROM journey")
    Journey[] getJourneyArray();
}
```

The previous interface example can be used in a JAVA application as follows :
``` java
DataSource ds = //... your postgresql datasource

try (SqlSession session = new SqlSession(ds)) {
    JourneyDao dao = session.getDao(JourneyDao.class);
    Journey[] journeys = dao.getJourneyArray();
}
```

## TODO
* Better support of handling exception
* Calling Stored Procedures
* Typesafe
* Multiple line
