package org.nuiton.spgeed;

import org.nuiton.spgeed.result.FacetResult;

public interface Facets {

    FacetResult getFacets();

    void setFacets(FacetResult facetResult);

}
