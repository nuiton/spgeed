package org.nuiton.spgeed.annotations;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.spgeed.SpgeedMapper;
import org.nuiton.spgeed.mapper.JsonMapper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use for SELECT statement.
 * You can use the parameter value from the method into SQL statement with "${parm.attribute}" syntax. 
 * The result from SQL statement is converted to the return type defined into the method.
 * 
 * @author julien
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Select {

    /**
     * @return SQL statement
     */
    public String sql();

    /**
     * @return limit of time to do the request
     */
    public String timeout() default "";

    /**
     * Spgeed use json format to retrieve data from postgresql server and convert
     * it to POJO or you can keep it to JSON.
     * You can switch to other mapper with this option (ex: simpleflatmapper)
     * @return mapper class to used
     */
    public Class<? extends SpgeedMapper> mapper() default SpgeedMapper.class;

    /**
     * @return SQL roles which can execute the SQL statement
     */
    public String[] roles() default {};
    
}
