package org.nuiton.spgeed.mapper.simpleflatmapper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpleFlatMapperConfig {

    /**
     * You must indicate column name that contains id, to
     * create 1-N relation correctly
     * @return column id name
     */
    public String[] ids() default {};


}
