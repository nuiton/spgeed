package org.nuiton.spgeed.mapper.simpleflatmapper;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.spgeed.AnnotationProvider;
import org.nuiton.spgeed.Chunk;
import org.nuiton.spgeed.ChunkArrayList;
import org.nuiton.spgeed.SpgeedMapper;
import org.nuiton.spgeed.SpgeedUtils;
import org.nuiton.spgeed.query.QueryFacets;
import org.postgresql.PGResultSetMetaData;
import org.simpleflatmapper.jdbc.JdbcMapper;
import org.simpleflatmapper.jdbc.JdbcMapperFactory;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimpleFlatMapper implements SpgeedMapper {

    private static final String FETCH_RESULT_ALIAS = "__fetch";
    private static final String FIRST_RESULT_ALIAS = "__first";
    private static final String NEXT_RESULT_ALIAS = "__next";
    private static final String TOTAL_RESULT_ALIAS = "__total";

    @Override
    public String getSql(AnnotationProvider annotationProvider, String sql, Optional<Chunk> optChunk, QueryFacets facets, Optional<String> roleContraints) {
        if (!facets.getFacets().isEmpty()) {
            throw new UnsupportedOperationException("SimpleFlatMapper don't support facets");
        }

        String result;

        String formattedRoles = roleContraints.map(r -> "WHERE " + r).orElse("");

        if (optChunk.isPresent()) {
            Chunk chunk = optChunk.get();
            result = String.format("WITH __all AS (%s) " +
                            "select" +
                            " g.*," +
                            " %s as %s," +                 // fetch size
                            " -1+min(g.__num__) over() as %s," + // first
                            " -1+max(g.__num__) over() as %s," +     // next
                            " from (select *, row_number() over () as __num__, count(*) over () as %s  from __all %s" +
                            " LIMIT %s OFFSET %s) g",
                    sql,
                    chunk.getFetch(), FETCH_RESULT_ALIAS,
                    FIRST_RESULT_ALIAS,
                    NEXT_RESULT_ALIAS,
                    TOTAL_RESULT_ALIAS,
                    formattedRoles,
                    chunk.getFetch(),
                    chunk.getNext());
        } else {
            result = String.format("WITH __all AS (%s) SELECT __all.* FROM __all %s",
                    sql, formattedRoles);
        }

        return result;
    }

    @Override
    public <E> E getResult(AnnotationProvider annotationProvider, ResultSet rs, Class<E> returnType, Class elementType) throws Exception {
        SimpleFlatMapperConfig config = (SimpleFlatMapperConfig)annotationProvider.getAnnotation(SimpleFlatMapperConfig.class);
        String[] ids = config.ids();

        EnhanceResultSetMetaData metaData =
                (EnhanceResultSetMetaData) Proxy.newProxyInstance(getClass().getClassLoader(),
                        new Class[]{EnhanceResultSetMetaData.class},
                        new ResultSetMetaDataHandler(rs.getMetaData(), elementType, ids));

        JdbcMapper<E> mapper = JdbcMapperFactory.newInstance()
                .addKeys(metaData.getAllColumnUid())
                .ignorePropertyNotFound()
                .newMapper(elementType, metaData);

        if (SpgeedUtils.returningChunk(returnType)) {
            EnhanceResultSet ers =
                    (EnhanceResultSet) Proxy.newProxyInstance(getClass().getClassLoader(),
                            new Class[]{EnhanceResultSet.class},
                            new ResultSetHandler(rs));

            return (E)mapper.stream(ers).collect(Collectors.toCollection(() -> ers.getChunk((Class<Chunk<E>>)returnType, elementType)));
        }

        if (SpgeedUtils.returningCollection(returnType)) {
            return (E)mapper.stream(rs).collect(Collectors.toCollection(() -> SpgeedUtils.newCollectionInstance(returnType)));
        }

        List result = mapper.stream(rs).collect(Collectors.toList());

        if (returnType.isArray()) {
            return (E) result.toArray((Object[]) Array.newInstance(elementType, result.size()));
        }

        if (!result.isEmpty()) {
            return (E)result.get(0);
        }

        return null;
    }

    protected interface EnhanceResultSetMetaData extends ResultSetMetaData {
        String[] getAllColumnUid();
    }

    protected interface EnhanceResultSet extends ResultSet {
        <T, C extends Chunk<T>> C getChunk(Class<C> c, Class<T> elementType);
    }

    protected static class ResultSetHandler implements InvocationHandler {

        protected ResultSet rs;
        protected Chunk chunk;

        public ResultSetHandler(ResultSet rs) {
            this.rs = rs;
        }

        public Chunk getChunk() {
            return chunk;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (StringUtils.equals(method.getName(), "getChunk")) {
                return SpgeedUtils.newChunk(Class.class.cast(args[0]), Class.class.cast(args[1]), chunk.getFetch(), chunk.getFirst(), chunk.getNext(), chunk.getTotal());
            } else if (StringUtils.equals(method.getName(), "next")) {
                boolean result = rs.next();
                if (result) {
                    chunk = createChunk(rs);
                }
                return result;
            }

            return method.invoke(rs, args);
        }

        protected Chunk createChunk(ResultSet rs) throws SQLException {
            long fetch = rs.getLong(FETCH_RESULT_ALIAS);
            long first = rs.getLong(FIRST_RESULT_ALIAS);
            long next = rs.getLong(NEXT_RESULT_ALIAS);
            long total = rs.getLong(TOTAL_RESULT_ALIAS);

            Chunk result = new ChunkArrayList(fetch, first, next, total);
            return result;
        }
    }

    protected static class ResultSetMetaDataHandler implements InvocationHandler {

        protected ResultSetMetaData meta;
        protected Class returnType;
        protected String[] ids;
        protected String[] allColumnUid;
        protected Set<String> allTableName;

        public ResultSetMetaDataHandler(ResultSetMetaData meta, Class returnType, String[] ids) {
            this.meta = meta;
            this.returnType = returnType;
            this.ids = ids;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (StringUtils.equals(method.getName(), "getColumnLabel")) {
                int columnIndex = (Integer) args[0];
                String result = getColumnLabel(columnIndex);
                return result;
            } else if (StringUtils.equals(method.getName(), "getAllColumnUid")) {
                return getAllColumnUid();
            }

            return method.invoke(meta, args);
        }

        protected String[] getAllColumnUid() throws SQLException {
            if (allColumnUid == null) {
                Set<String> result = new HashSet<>();
                int numCol = meta.getColumnCount();
                for (int i = 1; i <= numCol; i++) {
                    if (equalsIgnoreCase(meta.getColumnLabel(i), ids)) {
                        result.add(getColumnLabel(i));
                    }
                }
                allColumnUid = result.toArray(new String[result.size()]);
            }
            return allColumnUid;
        }

        protected String getColumnLabel(int columnIndex) throws SQLException {
            String label = meta.getColumnLabel(columnIndex);
            // getColumnName in pg drive call getColumnLabel :(
            String name = ((PGResultSetMetaData) meta).getBaseColumnName(columnIndex);
            String table = meta.getTableName(columnIndex);

            String result;
            if (!label.equalsIgnoreCase(name)) {
                // user force label, we use it
                result = label;
            } else if (isTable(label)) {
                // label is same as table, convert it to id table
                result = getTableIdColumn(label);
            } else if (returnType.getSimpleName().equalsIgnoreCase(table)) {
                // wanted returned type is same as table, we return label only
                // root object
                result = label;
            } else {
                // in all other case, we return table and label
                // inner object
                result = table + "_" + label;
            }

            return result;
        }

        protected String getTableIdColumn(String tableName) throws SQLException {
            String result = Stream.of(getAllColumnUid())
                    .filter(v -> StringUtils.startsWith(v, tableName + "_"))
                    .findAny().orElseThrow(() -> new IllegalArgumentException("Can't find id for table: " + tableName));

            return result;
        }

        protected boolean isTable(String name) throws SQLException {
            return getAllTableName().contains(name);
        }

        protected Set<String> getAllTableName() throws SQLException {
            if (allTableName == null) {
                Set<String> result = new HashSet<>();
                int numCol = meta.getColumnCount();
                for (int i = 1; i <= numCol; i++) {
                    result.add(meta.getTableName(i));
                }
                allTableName = result;
            }
            return allTableName;
        }

        protected boolean equalsIgnoreCase(String s, String ... m) {
            return Stream.of(m).anyMatch(v -> s.equalsIgnoreCase(v));
        }

    }

}
