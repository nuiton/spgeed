package org.nuiton.spgeed.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.json.simple.JSONArray;
import org.nuiton.spgeed.AnnotationProvider;
import org.nuiton.spgeed.Chunk;
import org.nuiton.spgeed.Facets;
import org.nuiton.spgeed.SpgeedMapper;
import org.nuiton.spgeed.SpgeedUtils;
import org.nuiton.spgeed.query.QueryFacet;
import org.nuiton.spgeed.query.QueryFacets;
import org.nuiton.spgeed.result.FacetResult;

import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.ResultSet;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JsonMapper implements SpgeedMapper {

    private static final String JSON_RESULT_ALIAS = "__json";
    private static final String FETCH_RESULT_ALIAS = "__fetch";
    private static final String FIRST_RESULT_ALIAS = "__first";
    private static final String NEXT_RESULT_ALIAS = "__next";
    private static final String TOTAL_RESULT_ALIAS = "__total";
    private static final String FACETS_RESULT_ALIAS = "__facets";

    public void configureObjectMapper(ObjectMapper mapper) {
        // there is no case in SQL, but in java with love camelCase :p
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        SimpleModule module = new SimpleModule();
        module.addDeserializer(byte[].class, new JsonDeserializer<byte[]>() {
            @Override
            public byte[] deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
                String value = jsonParser.getText();
                byte[] decode = parseHexBinary(value.substring(2)); // Remove \x
                return decode;
            }
        });
        mapper.registerModule(module);
    }

    @Override
    public String getSql(AnnotationProvider annotationProvider, String sql, Optional<Chunk> optChunk, QueryFacets facets, Optional<String> roleContraints) {
        String result;

        String formattedRoles = roleContraints.map(r -> "WHERE " + r).orElse("");

        if (optChunk.isPresent()) {
            Chunk chunk = optChunk.get();
            result = String.format("WITH __all AS (%s) " +
                            "select" +
                            " to_json(array_agg(to_jsonb(g) - '{__num__, __total__}'::text[])) as %s," +
                            " %s as %s," +                 // fetch size
                            " min(g.__num__) - 1 as %s," + // first
                            " max(g.__num__) as %s," +     // next
                            " max(g.__total__) as %s" +    // total
                            " %s" + // les facets
                            " from (select *, row_number() over () as __num__, count(*) over () as __total__  from __all %s" +
                            " LIMIT %s OFFSET %s) g",
                    sql,
                    JSON_RESULT_ALIAS,
                    chunk.getFetch(), FETCH_RESULT_ALIAS,
                    FIRST_RESULT_ALIAS,
                    NEXT_RESULT_ALIAS,
                    TOTAL_RESULT_ALIAS,
                    generateFacets(facets, "__all", FACETS_RESULT_ALIAS),
                    formattedRoles,
                    chunk.getFetch(),
                    chunk.getNext());
        } else {
            result = String.format("WITH __all AS (%s) SELECT json_agg(__all.*) as %s %s FROM __all %s", sql, JSON_RESULT_ALIAS, generateFacets(facets, "__all", FACETS_RESULT_ALIAS), formattedRoles);
        }
        return result;
    }

    protected String generateFacets(QueryFacets facets, String view, String facetsAlias) {
        StringJoiner result = facets.getFacets().stream()
                .map(facet -> String.format("select '%s' as facet, jsonb_object_agg(topic, count) as topics from (%s) as %1$s", facet.getName(), facet.getQuery(view)))
                .collect(
                        () -> new StringJoiner(" UNION ALL ", ", (select jsonb_object_agg(facet, topics) from (", ") allFacets) as " + facetsAlias).setEmptyValue(""),
                        StringJoiner::add,
                        StringJoiner::merge
                );
        return result.toString();
    }

    @Override
    public  <E> E getResult(AnnotationProvider annotationProvider, final ResultSet rs, Class<E> returnType, Class elementType) throws Exception {
        while (rs.next()) { // only one result, because json is aggregate
            String json = rs.getString(JSON_RESULT_ALIAS);
            if (json == null) {
                return null;
            }

            ObjectMapper mapper = new ObjectMapper();
            configureObjectMapper(mapper);

            Stream stream;
            if (returnType.getSimpleName().equals("byte[]")) {
                return (E) mapper.readValue(json, JSONArray.class).stream()
                        .flatMap(m -> ((Map) m).values().stream())
                        .findFirst()
                        .map(v -> parseHexBinary(String.valueOf(v).substring(2))) // Remove \x
                        .orElse(null);
            } else if (JSONArray.class.isAssignableFrom(returnType)) {
                return (E) mapper.readValue(json, JSONArray.class);
            } else if (SpgeedUtils.returningPrimitive(elementType)) {
                stream = mapper.readValue(json, JSONArray.class).stream()
                        .flatMap(m -> ((Map) m).values().stream());

                if (Number.class.isAssignableFrom(elementType)) {
                    stream = stream.map(v -> SpgeedUtils.convertToNumber(v, elementType));
                } else if (elementType.equals(UUID.class)) {
                    stream = stream.map(v -> UUID.fromString((String) v));
                } else if (elementType.isEnum()) {
                    stream = stream.map(v -> Enum.valueOf(elementType, (String) v));
                }
            } else {
                String elementTypeName = elementType.getName();
                Class<?> returnTypeArray = findElementArrayClass(elementTypeName);
                Object[] readValue = (Object[]) mapper.readValue(json, returnTypeArray);
                stream = Stream.of(readValue);
            }

            E result;
            if (SpgeedUtils.returningCollection(returnType)) {
                result = (E) stream.collect(Collectors.toCollection(() -> SpgeedUtils.newCollectionInstance(returnType)));
            } else if (SpgeedUtils.returningArray(returnType)) {
                Object[] tmp = stream.toArray();
                Object array = Array.newInstance(elementType, tmp.length);
                // System.arraycopy don't work for primitive
                for (int i = 0, max = tmp.length; i < max; i++) {
                    Array.set(array, i, tmp[i]);
                }
                result = (E) array;
            } else {
                result = (E) stream.findFirst().orElse(null);
            }

            if (SpgeedUtils.returningChunk(returnType)) {
                long fetch = rs.getLong(FETCH_RESULT_ALIAS);
                long first = rs.getLong(FIRST_RESULT_ALIAS);
                long next = rs.getLong(NEXT_RESULT_ALIAS);
                long total = rs.getLong(TOTAL_RESULT_ALIAS);

                Chunk.class.cast(result).set(elementType, fetch, first, next, total);
            }

            if (SpgeedUtils.returningFacets(returnType)) {
                String facets = rs.getString(FACETS_RESULT_ALIAS);
                FacetResult facetResult = mapper.readValue(facets, FacetResult.class);

                Facets.class.cast(result).setFacets(facetResult);
            }

            return result;
        }
        return null;
    }

    /**
     * Find a Class for the given elementTypeName as an Array.
     *
     * This method is meant to be overriden if Spgeed is used in a framework with a custom classpath management
     * (eg. Quarkus's dev mode). Don't make it private.
     *
     * @param elementTypeName the element to find in an array format. Example: {@code MyObject}
     * @return the array's Class. Example: {@code Class<MyObject[]>}
     * @throws ClassNotFoundException if the class cannot be found
     * @see Class#forName(String)
     */
    protected Class<?> findElementArrayClass(String elementTypeName) throws ClassNotFoundException {
        String arrayClassName = "[L" + elementTypeName + ";";
        Class<?> result = Class.forName(arrayClassName);
        return result;
    }

    // from jaxb implementation (javax.xml.bind.DatatypeConverterImpl#parseHexBinary)
    private byte[] parseHexBinary(String s) {
        final int len = s.length();

        // "111" is not a valid hex encoding.
        if (len % 2 != 0) {
            throw new IllegalArgumentException("hexBinary needs to be even-length: " + s);
        }

        byte[] out = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            int h = hexToBin(s.charAt(i));
            int l = hexToBin(s.charAt(i + 1));
            if (h == -1 || l == -1) {
                throw new IllegalArgumentException("contains illegal character for hexBinary: " + s);
            }

            out[i / 2] = (byte) (h * 16 + l);
        }

        return out;
    }

    // from jaxb implementation (javax.xml.bind.DatatypeConverterImpl#parseHexBinary)
    private int hexToBin(char ch) {
        if ('0' <= ch && ch <= '9') {
            return ch - '0';
        }
        if ('A' <= ch && ch <= 'F') {
            return ch - 'A' + 10;
        }
        if ('a' <= ch && ch <= 'f') {
            return ch - 'a' + 10;
        }
        return -1;
    }

}
