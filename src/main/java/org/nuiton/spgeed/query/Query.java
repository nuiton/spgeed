package org.nuiton.spgeed.query;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import jodd.bean.BeanException;
import jodd.bean.BeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.spgeed.AnnotationProvider;
import org.nuiton.spgeed.Chunk;
import org.nuiton.spgeed.SpgeedMapper;
import org.nuiton.spgeed.SpgeedUtils;
import org.nuiton.spgeed.SqlSession;
import org.nuiton.spgeed.mapper.JsonMapper;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents the statement which can be execute on the current transaction.
 * 
 * @author julien
 */
public class Query {
    
    final static private Log log = LogFactory.getLog(Query.class);

    protected SqlSession session;
    
    protected String sql;
    protected Class mapper;
    protected AnnotationProvider annotationProvider;
    protected String[] roles;
    protected Map<String, Object> parameters;
    protected Class returnType;
    protected Class elementType;
    
    protected String parsedSql;
    protected List<Object> sqlParameters;

    /**
     *
     * @param session session to use
     * @param sql sql request as string
     * @param mapper mapper to use (default: json)
     * @param annotationProvider permit to retrieve annotation mapper config
     * @param roles Role authorized to execute this query
     * @param parameters parameters of sql request
     * @param returnClass wanted returned type
     * @throws Exception
     */
    public Query(SqlSession session, String sql, Class mapper, AnnotationProvider annotationProvider, String[] roles, Map<String, Object> parameters, Class<?> returnClass, Type returnType) throws Exception {
        this.session = session;
        
        this.sql = sql;
        this.mapper = mapper;
        this.annotationProvider = annotationProvider;
        this.roles = roles;
        this.parameters = parameters;
        this.returnType = returnClass;

        this.sqlParameters = new ArrayList<>();

        elementType = SpgeedUtils.getElementType(returnType).orElse(returnClass);
    }

    public Query(SqlSession session, String sql, Map<String, Object> parameters, Class<?> returnClass, String... roles) throws Exception {
        this(session, sql, null, type -> null, roles, parameters, returnClass, returnClass);
    }

    public String getParsedSql() throws SQLException {
        if (this.parsedSql == null) {
            try {
                this.parsedSql = this.parseParameters(this.sql);
            } catch (Exception eee) {
                throw new SQLException("Can't parse sql: " + this.sql, eee);
            }
        }
        return parsedSql;
    }

    protected String parseParameters(String sql) throws Exception {
        String result = QueryParser.parse(sql, this);
        return result;
    }

    public Object evalField(String path) {
        Object result = this.evalField(this.parameters, path);
        return result;
    }

    /**
     * Extrait la valeur du champs pointé par path, si durant la navigation
     * dans le path une des composantes est null, alors null est retourné.
     *
     * @param o l'objet de de depart
     * @param path le path (ex: "toto.titi.truc[2].bidulle")
     * @return l'objet pointé ou null, une erreur est levé si une des propriétés n'existe pas
     */
    public Object evalField(Object o, String path) {
        Object result = o;
        if (StringUtils.isNotBlank(path)) {
            String queue = path;

            for(int next = this.indexOfDot(queue); next >= 0 && result != null; next = this.indexOfDot(queue)) {
                String prop = queue.substring(0, next);
                queue = queue.substring(next + 1);
                result = this.evalOneField(result, prop);
            }

            result = this.evalOneField(result, queue);
        }
        return result;
    }

    private Object evalOneField(Object o, String prop) {
        Object result = null;
        if (o != null) {
            try {
                result = BeanUtil.pojo.getProperty(o, prop);
            } catch (BeanException eee) {
                throw new RuntimeException(String.format("Can't find properties '%s' in '%s'", prop, o), eee);
            }
        }
        return result;
    }

    /**
     * Finds the very first next dot. Ignores dots between index brackets.
     * Returns <code>-1</code> when dot is not found.
     */
    protected int indexOfDot(final String name) {
        int ndx = 0;
        int len = name.length();
        boolean insideBracket = false;

        while (ndx < len) {
            char c = name.charAt(ndx);
            if (insideBracket) {
                if (c == ']') {
                    insideBracket = false;
                }
            } else {
                if (c == '.') {
                    return ndx;
                }
                if (c == '[') {
                    insideBracket = true;
                }
            }
            ndx++;
        }
        return -1;
    }


    public Object evalFunction(Object value, String name, List args) {
        try {
            Map<String, PipeFunction> pipeFunctions = this.session.getPipeFunctions();
            PipeFunction pipeFunction = pipeFunctions.get(name);
            if (pipeFunction == null) {
                throw new RuntimeException(String.format("Can find function for evalFunction(%s, '%s', %s)", value, name, args));
            }

            Object result = pipeFunction.function(this, value, args == null ? null : args.toArray());
            return result;
        } catch (Exception eee) {
            throw new RuntimeException(String.format("Error during evalFunction(%s, %s, %s)", value, name, args), eee);
        }
    }

    /**
     *
     * @param value object to used as parameter for sql query
     * @return string will must be used to replace argument in sql string
     */
    public String addSqlParameter(Object value) {
        this.sqlParameters.add(value);
        return "?";
    }

    public SqlSession getSession() {
        return session;
    }

    protected PreparedStatement getStatement(String sql) throws SQLException {
        Connection connection = this.session.getConnection();

        PreparedStatement statement = connection.prepareStatement(sql);

        int position = 1;
        for (Object arg : this.sqlParameters) {
            if (arg != null && arg.getClass().isEnum()) {
                statement.setObject(position++, arg.toString());
            } else {
                statement.setObject(position++, arg);
            }
        }
        return statement;
    }

    protected Optional<String> getFormattedRoles() {
        Optional<String> result = Optional.empty();
        if (roles != null && roles.length > 0) {
            String formattedRoles = Stream.of(this.roles)
                    .collect(Collectors.joining("', 'member') OR pg_has_role('", "pg_has_role('", "', 'member')"));
            result = Optional.of(formattedRoles);
        }
        return result;
    }

    /**
     * Returns the mapper class. If the current query does not have a mapper, use the default one declared on the
     * {@link SqlSession}
     *
     * @return the mapper class
     */
    protected Class safeGetMapper() {
        if (mapper == null) {
            return session.getDefaultMapper();
        } else {
            return mapper;
        }
    }

    public<E> E executeQuery() throws Exception {
        String sql = this.getParsedSql();

        Optional<Chunk> chunk = Optional.empty();
        if (SpgeedUtils.returningChunk(returnType)) {
            chunk = Optional.of(parameters.values().stream()
                    .filter(p -> p instanceof Chunk)
                    .map(Chunk.class::cast)
                    .findFirst().orElseThrow(() -> new IllegalArgumentException("Missing Chunk parameter")));
        }

        QueryFacets facets = new QueryFacets();
        for (Object arg : parameters.values()) {
            if (arg instanceof QueryFacets) {
                facets.addFacets((QueryFacets) arg);
            } else if (arg instanceof QueryFacet) {
                facets.addFacets((QueryFacet) arg);
            } else if (arg instanceof QueryFacet[]) {
                facets.addFacets((QueryFacet[]) arg);
            }
        }

        SpgeedMapper m = SpgeedMapper.class.cast(safeGetMapper().getDeclaredConstructor().newInstance());
        sql = m.getSql(annotationProvider, sql, chunk, facets, getFormattedRoles());
        try (PreparedStatement statement = this.getStatement(sql)) {
            try (ResultSet rs = statement.executeQuery()) {
                E result;
                if (this.returnType.equals(Void.TYPE)) {
                    result = null;
                } else if (this.returnType.equals(ResultSet.class)) {
                    result = (E) rs;
                } else {
                    result = (E)m.getResult(annotationProvider, rs, returnType, elementType);
                }

                return result;
            }
        } catch (Exception eee) {
            log.error(String.format("Can't execute query '%s' with args: %s", this.getParsedSql(), this.sqlParameters));
            throw eee;
        }
    }
    
    public <E> E executeUpdate() throws Exception {
        if (sql.contains("RETURNING")) {
            return executeQuery();
        }

        String sql = this.getParsedSql();
        try (PreparedStatement statement = this.getStatement(sql)) {
            Integer result = statement.executeUpdate();
            return (E) result;

        } catch (Exception eee) {
            log.error(String.format("Can't execute query '%s' with args: %s", this.getParsedSql(), this.sqlParameters));
            throw eee;
        }
    }

    public boolean execute() throws Exception {
        String sql = this.getParsedSql();
        
        try (PreparedStatement statement = this.getStatement(sql)) {
            boolean result = statement.execute();
            return result;
            
        } catch (Exception eee) {
            log.error(String.format("Can't execute query '%s' with args: %s", this.getParsedSql(), this.sqlParameters));
            throw eee;
        }
    }

}
