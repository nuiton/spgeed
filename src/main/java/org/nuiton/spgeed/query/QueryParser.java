package org.nuiton.spgeed.query;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import org.parboiled.BaseParser;
import org.parboiled.Context;
import org.parboiled.Parboiled;
import org.parboiled.Rule;
import org.parboiled.annotations.BuildParseTree;
import org.parboiled.errors.ErrorUtils;
import org.parboiled.parserunners.ReportingParseRunner;
import org.parboiled.support.ParsingResult;
import org.parboiled.support.StringVar;
import org.parboiled.support.Var;

/**
 * Parser to replace ${} by a right value (ie ? or a string) and extract SQL parameters.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@BuildParseTree
public class QueryParser extends BaseParser<Object> {

    static public String parse(String sql, Query query) {
        QueryParser parser = Parboiled.createParser(QueryParser.class);

        Var<Query> qv = new Var<>(query);

        ParsingResult<Object> result = new ReportingParseRunner<Object>(parser.start(qv)).run(sql);
//        ParsingResult<Object> result = new TracingParseRunner<Object>(parser.start(qv)).run(sql);
//        ParsingResult<Object> result = new RecoveringParseRunner<Object>(parser.start(qv)).run(sql);

        if (result.hasErrors() || !result.matched) {
            System.out.println("\nParse Errors:\n" + ErrorUtils.printParseErrors(result));
        }

//        System.out.println("\nParse Tree:\n" + ParseTreeUtils.printNodeTree(result) + '\n');

        return (String)result.resultValue;
    }

    boolean append() {
        Context context = getContext();
        boolean result = true;
        if (context.getLevel() < 5) {
            String s = String.valueOf(pop());
//            System.out.println(String.format("append to: '%s' add '%s'", peek(), s));
            result = push(pop() + s);
        }
        return result;
    }

    /**
     * add call to 'toParam' function only for root expression (low level)
     * @param hasFunction true if expression has already function
     * @return true if all done
     */
    boolean addDefaultFunction(boolean hasFunction) {
        Context context = getContext();
        boolean result = true;
        if (context.getLevel() < 5 && !hasFunction) {
            result = push(query.evalFunction(pop(), "toParam", null));
        }
        return result;
    }

    Number toNumber(String s) {
        Number result;
        if (s.matches("[0-9]+")) {
            result = Integer.valueOf(s);
        } else {
            result = Double.valueOf(s);
        }
        return result;
    }

    Query query;
    public Rule start(Var<Query> qv) {
        this.query = qv.get();
        return Sequence(push(""),
                OneOrMore(
                        FirstOf(
                                Sequence(
                                        verbatim(),
                                        push(match()),
                                        append()
                                ),
                                expression()
                        )
                ),
                EOI
        );
    }

    Rule expression() {
        Var<Boolean> hasFunction = new Var<>(false);
        return Sequence(
                "${",
                space(),
                field(),
                space(),
                ZeroOrMore(
                        "|",
                        space(),
                        function(),
                        space(),
                        hasFunction.set(true)
                ),
                "}",
                addDefaultFunction(hasFunction.get()),
                append()
        );
    }

    Rule function() {
        StringVar name = new StringVar();
        Var<List> args = new Var<>(new ArrayList());
        return Sequence(
                functionName(),
                name.set(match()),
                space(),
                "(",
                space(),
                Optional(functionParams(args)),
                ")",
                push(query.evalFunction(pop(), name.get(), args.get()))
        );
    }

    Rule field() {
        return Sequence(
                OneOrMore(FirstOf(CharRange('0', '9'), CharRange('a', 'z'), CharRange('A', 'Z'), AnyOf("_.[]"))),
                push(query.evalField(match()))
                );
    }

    Rule functionName() {
        return OneOrMore(FirstOf(CharRange('0', '9'), CharRange('a', 'z'), CharRange('A', 'Z'), AnyOf("_")));
    }

    Rule functionParams(Var<List> args) {
        return Sequence(
                functionParam(args),
                ZeroOrMore(
                        ",",
                        functionParam(args)
                )
        );
    }

    Rule functionParam(Var<List> args) {
        return Sequence(
                space(),
                FirstOf(string(), number(), expression(), function(), field()),
                args.get().add(pop()),
                space());
    }

    Rule string() {
        return FirstOf(stringSimple(), stringDouble());
    }

    Rule stringDouble() {
        return Sequence(
                '"',
                ZeroOrMore(
                        FirstOf(
                                escape(),
                                Sequence(TestNot(AnyOf("\r\n\"\\")), ANY)
                        )
                ).suppressSubnodes(),
                push(match()),
                '"'
        );
    }

    Rule stringSimple() {
        return Sequence(
                '\'',
                ZeroOrMore(
                        FirstOf(
                                escape(),
                                Sequence(TestNot(AnyOf("\r\n\'\\")), ANY)
                        )
                ).suppressSubnodes(),
                push(match()),
                '\''
        );
    }

    Rule escape() {
        return Sequence('\\', FirstOf(AnyOf("btnfr\"\'\\"), octalEscape(), unicodeEscape()));
    }

    Rule octalEscape() {
        return FirstOf(
                Sequence(CharRange('0', '3'), CharRange('0', '7'), CharRange('0', '7')),
                Sequence(CharRange('0', '7'), CharRange('0', '7')),
                CharRange('0', '7')
        );
    }

    Rule unicodeEscape() {
        return Sequence(OneOrMore('u'), hexDigit(), hexDigit(), hexDigit(), hexDigit());
    }

    Rule hexDigit() {
        return FirstOf(CharRange('a', 'f'), CharRange('A', 'F'), CharRange('0', '9'));
    }


    Rule number() {
        return Sequence(
                OneOrMore(
                        digit(),
                        Optional(
                                '.',
                                digit()
                        )
                ),
                push(toNumber(match()))
        );
    }

    Rule verbatim() {
        return OneOrMore(TestNot("${"), ANY);
    }

    Rule space() {
        return ZeroOrMore(AnyOf(" \t\f"));
    }

    Rule digit() {
        return CharRange('0', '9');
    }

}
