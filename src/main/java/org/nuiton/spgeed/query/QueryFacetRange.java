package org.nuiton.spgeed.query;

/**
 * Le rangeGenerator doit resortir 3 champs: range, r_min, r_max
 * Il est possible d'utiliser la valeur special __all qui contient toutes les valeurs de la requete courante
 *
 * range est le nom qui sera donné au topic
 * r_min et r_max sont les valeurs min et max du range
 * La comparaison se fait de la facon suivant: r_min <= value < r_max
 */
public class QueryFacetRange extends QueryFacet {

    protected String field;
    /**
     * can be simply series: SELECT (serieValue*5)::text||'-'||(serieValue*5+4)::text AS range, serieValue*5 AS r_min, serieValue*5+4 AS r_max FROM generate_series(5, 10) AS serieValue
     * or union of static value: SELECT '-24' AS range, double precision '-Infinity' AS r_min, 24 AS r_max UNION ALL SELECT '25-50' AS range, 25 AS r_min, 50 AS r_max UNION ALL SELECT '51+' AS range, 51 AS r_min, double precision 'Infinity' AS r_max
     * or both: SELECT '-24' AS range, double precision '-Infinity' AS r_min, 24 AS r_max UNION ALL SELECT (serieValue*5)::text||'-'||(serieValue*5+4)::text AS range, serieValue*5 AS r_min, serieValue*5+4 AS r_max FROM generate_series(5, (SELECT max(myintfield)/5 FROM __all)) AS serieValue
     */
    protected String rangeGenerator;

    public QueryFacetRange(String name, String rangeGenerator) {
        super(name);
        this.rangeGenerator = rangeGenerator;
    }

    @Override
    public String getQuery(String tableSource) {
        return String.format("select r.range as topic, count(f.*) as count from (%s AS rangeQuery) r left join %s f on r.r_min <= f.%s and f.%s < r.r_max group by topic order by %s limit %s", rangeGenerator, tableSource, getSort().getSortClause(), field, getMax());
    }

}
