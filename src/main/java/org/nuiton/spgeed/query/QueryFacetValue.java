package org.nuiton.spgeed.query;

public class QueryFacetValue extends QueryFacet {

    /**
     * can be simply the field name: myfield
     * or sql expression: unnest(myarrayfield)
     * or sql date expression: date_trunc('month', mydatefield)
     * or sql date expression: date_part('month', mydatefield)
     */
    protected String valueExtractor;

    public QueryFacetValue(String name, String valueExtractor) {
        super(name);
        this.valueExtractor = valueExtractor;
    }

    @Override
    public String getQuery(String tableSource) {
        return String.format("select %s as topic, count(*) as count from %s group by topic order by %s, topic limit %s", valueExtractor, tableSource, getSort().getSortClause(), getMax());
    }
}
