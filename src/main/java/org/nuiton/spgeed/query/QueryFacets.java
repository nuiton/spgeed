package org.nuiton.spgeed.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class QueryFacets {

    protected List<QueryFacet> facets = Collections.emptyList();

    public List<QueryFacet> getFacets() {
        return facets;
    }

    public void addFacets(QueryFacet... facet) {
        if (this.facets == Collections.EMPTY_LIST) {
            this.facets = new ArrayList<>();
        }
        this.facets.addAll(Arrays.asList(facet));
    }

    public void addFacets(QueryFacets facets) {
        if (this.facets == Collections.EMPTY_LIST) {
            this.facets = new ArrayList<>();
        }
        this.facets.addAll(facets.getFacets());
    }

    public void setFacets(List<QueryFacet> facets) {
        if (facets == null) {
            this.facets = Collections.emptyList();
        } else {
            this.facets = facets;
        }
    }
}
