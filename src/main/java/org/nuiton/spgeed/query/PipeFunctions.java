package org.nuiton.spgeed.query;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.postgresql.util.PGobject;

/**
 * Default function that can be used in sql expression (${...}).
 *
* If you create new function here, add it in getExposedFunction
 *
 * @author julien
 */
public class PipeFunctions {

    protected Map<String, PipeFunction> exposedFunction;

    public PipeFunctions() {
    }

    public Map<String, PipeFunction> getExposedFunction() {
        if (exposedFunction == null) {
            Map<String, PipeFunction> result = new HashMap<>();
            result.put("toString", this::toString);
            result.put("toParam", this::toParam);
            result.put("json", this::json);
            result.put("array", this::array);
            result.put("values", this::values);
            result.put("map", this::map);
            result.put("row", this::row);
            result.put("collection", this::collection);
            result.put("cast", this::cast);
            result.put("raw", this::raw);
            exposedFunction = result;
        }
        return exposedFunction;
    }

    static interface MapFunction {
        Object map(Object o) throws Exception;
    }

    /**
     * Call f.map for each element in value. If value is not a collection apply f only to this value
     * @param forceCollection if true result is always a collection.
     * if value is null return empty collection
     * if value is object return collection with one item
     * if value is array or collection return collection with same item number
     * @param value Collection or Array or Object
     * @param f function used to map object
     * @return null if value is null, Object return by f if value is not a collection
     * and new collection with mapped result otherwize
     */
    protected Object map(boolean forceCollection, Object value, MapFunction f) throws Exception {
        Object result;
        if (value == null) {
            if (forceCollection) {
                result = Collections.EMPTY_LIST;
            } else {
                result = null;
            }
        } else if (value.getClass().isArray()) {
            Collection list = new ArrayList();
            result = list;
            for (Object o : (Object[])value) {
                list.add(f.map(o));
            }
        } else if (value instanceof Collection) {
            Collection list = new ArrayList();
            result = list;
            for (Object o : (Collection)value) {
                list.add(f.map(o));
            }
        } else {
            result = f.map(value);
            if (forceCollection) {
                result = Arrays.asList(result);
            }
        }
        return result;
    }

    /**
     * try to match multiple fields with comma separator
     * ex: "bidulle.truc, bidulle.machin, bidulle.email[0]"
     *
     * @param fields
     * @return true if fields is list of field
     */
    protected boolean isRow(String fields) {
        // match: bidulle.address[0].postalcode::int
        String fieldPath = "\\p{Alpha}\\p{Alnum}*(?:\\[\\p{Digit}+\\])?(?:\\.\\p{Alpha}\\p{Alnum}*(?:\\[\\p{Digit}+\\])?)*(?:::\\p{Alpha}\\p{Alnum}*)?";
        boolean result = fields != null &&
                fields.matches("^\\s*(" + fieldPath + ")\\s*(?:,\\s*(" + fieldPath + ")\\s*)+$");
        return result;
    }

    /**
     *
     * @param field
     * @param appendDelim if true, append "::" before type
     * @return return field name and field type, if no field name or type, empty string is returned in array
     */
    protected String[] getFieldType(String field, boolean appendDelim) {
        String[] result = {"", ""};
        if (StringUtils.isNotBlank(field)) {
            String[] split = field.split("::", 2);
            result[0] = split[0];
            if (split.length > 1) {
                if (appendDelim) {
                    result[1] += "::";
                }
                result[1] += split[1];
            }
        }
        return result;
    }

    /**
     * Add value to query parameter and return string substitution '?'
     * @param query current query
     * @param in value to convert to param
     * @param args only one argument. This argument must be cast per example
     * "::json", is used to add "::json" (type must be postgresql type)
     * you can specify field name before type, to extract only one field from in value
     * ex: birthdate::int
     * @return string substitution '?'
     * @throws java.lang.Exception
     */
    public Object toParam(Query query, Object in, Object ... args) throws Exception {
        Object result;
        String fieldName = null;

        if (args != null) {
            if (args.length > 1) {
                throw new IllegalArgumentException(
                        "Only zero or one argument is allowed for toParam function. Invalide argument is: "
                                + Arrays.toString(args));
            }

            if (args.length > 0) {
                fieldName = (String)args[0];
            }
        }

        if (this.isRow(fieldName)) {
            result = this.row(query, in, (Object[])fieldName.split(","));
        } else {
            String[] fieldType = this.getFieldType(fieldName, true);
            Object value = query.evalField(in, fieldType[0]);

            if (value == null
                    || value instanceof String
                    || value instanceof Integer
                    || value instanceof Float
                    || value instanceof BigDecimal
                    || value instanceof Boolean
                    || value instanceof UUID
                    || value instanceof Date
                    || value instanceof Timestamp
                    || value instanceof LocalDate
                    || value instanceof LocalDateTime
                    || value instanceof OffsetDateTime
                    || value instanceof Double
                    || value.getClass().getSimpleName().equals("byte[]")) {

                result = query.addSqlParameter(value);

            } else if (value.getClass().isEnum()) {
                result = query.addSqlParameter(value);

            } else if (value instanceof Collection || value.getClass().isArray()) {
                result = this.array(query, value);

            } else {
                result = this.json(query, value);
            }

            // add cast if present
            result += fieldType[1];
        }
        return result;
    }

    /**
     * convert value to string value and return it
     * @param query current query
     * @param in value to convert to string
     * @param args not used
     * @return string representation of value
     */
    public Object toString(Query query, Object in, Object ... args) {
        return String.valueOf(in);
    }

    /**
     * convert value to json and add it to query parameter
     * @param query current query
     * @param in value to convert to json
     * @param args not used
     * @return string parameter substitution '?'
     * @throws JsonProcessingException
     * @throws SQLException
     */
    public Object json(Query query, Object in, Object ... args) throws JsonProcessingException, SQLException {
        PGobject pg = new PGobject();
        pg.setType("JSON");
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        /** il faut mettre en minuscule, car sinon postresql ne comprend pas les fields CamelCase */
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CASE);
        /** sinon on ne peut jamais utiliser les defaults value mis dans la declaration de table */
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);


        String value = mapper.writeValueAsString(in);
        pg.setValue(value);
        
        String result = query.addSqlParameter(pg);
        return result;
    }

    /**
     * Create string SQL Array representation of value, where each array item
     * is replace by '?' and real value pushed in query parameter
     * @param query current query
     * @param in Must be Collection or Array, each element is one element in sql ARRAY
     * @param args optional, use to cast each value in array
     * @return SQL array as string with parameter (ex: ARRAY[?, ?, ?])
     */
    public Object array(Query query, Object in, Object ... args) throws Exception {
        if (in == null) {
            in = Collections.emptyList();
        }

        StringJoiner joiner = new StringJoiner(", ", "ARRAY[", "]");


        Collection c;
        if (args != null && args.length > 0) {
            joiner.setEmptyValue("ARRAY[]::" + args[0] + "[]");
            c = (Collection)map(true, in, o -> this.cast(query, o, args));
        } else {
            c = (Collection)map(true, in, o -> this.toParam(query, o));
        }
        c.forEach(v -> joiner.add((String)v));

        String result = joiner.toString();
        return result;
    }

    public Object collection(Query query, Object in, Object ... args) throws Exception {
        String result = "(";
        if (in != null) {
            Collection c = (Collection)map(true, in, o -> this.toParam(query, o));
            result += String.join(", ", c);
        }
        result += ")";
        return result;
    }

    public Object row(Query query, Object in, Object ... args) throws Exception {
        String result = "ROW" + values(query, in, args);
        return result;
    }

    /**
     * create sql insert value string, support 'in' as object or collection
     * ex: "insert into mytable (id, myfield) values ${myobject |values('id', 'myfield)}"
     * return "insert into mytable (id, myfield) values (?, ?)"
     *
     * "insert into mytable (id, myfield) values ${myArrayOfObject |values('id', 'myfield)}"
     * return "insert into mytable (id, myfield) values (?, ?), (?, ?), (?, ?)"
     *
     * @param query current query
     * @param in object or collection of object used to create values
     * @param args list of field to extract as value parameter
     * @return
     */
    public Object values(Query query, Object in, Object ... args) throws Exception {
        MapFunction fn = (o) -> {
            // use map to contains all field
            List values = new ArrayList<>();
            for (Object a : args) {
                Object parameter;
                if (a instanceof String) {
                    String name = (String)a;
                    if (name.isEmpty()) {
                        parameter = toParam(query, o);
                    } else {
                        parameter = toParam(query, o, name);
                    }
                } else {
                    parameter = toParam(query, a);
                }
                    
                values.add(parameter);
            }

            return "(" + String.join(", ", values) + ")";
        };

        Object result = map(false, in, fn);
        if (result instanceof Collection) {
            result = String.join(", ", ((Collection)result));
        }
        return result;
    }
    
    /**
     * Convert each value in parameter 'in'
     * @param query current query
     * @param in Collection to convert
     * @param args field to extract from each object in parameter 'in'
     * @return collection with extracted field, if arg is empty, return in
     * if args length == 1 then collection contains field value directly
     * if args length >= 2 then collection contains map with field name as key
     */
    public Object map(Query query, Object in, Object ... args) throws Exception {
        if (args == null || args.length == 0) {
            return in;
        }
        Collection result = (Collection)map(true, in, (o) -> {
            if (args.length == 1) {
                // add directly field in result
                return query.evalField(o, String.valueOf(args[0]));
            } else {
                // use map to contains all field
                Map<String, Object> values = new HashMap<>();
                for (Object a : args) {
                    String name = String.valueOf(a);
                    Object value = query.evalField(o, name);
                    values.put(name, value);
                }
                return values;
            }
        });
        return result;
    }

    public Object cast(Query query, Object in, Object ... args) throws Exception {
        Object result;
        Object tmp = map(false, in, o -> this.toParam(query, o));
        if (tmp instanceof Collection) {
            result = Collection.class.cast(tmp).stream().map(o -> o + "::" + args[0]).collect(Collectors.toList());
        } else {
            result = tmp + "::" + args[0];
        }
        return result;
    }

    public Object raw(Query query, Object in, Object ... args) throws Exception {
        return String.valueOf(in);
    }


}
