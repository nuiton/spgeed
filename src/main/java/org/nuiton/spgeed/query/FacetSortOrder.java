package org.nuiton.spgeed.query;

import javax.swing.*;

public enum FacetSortOrder {
    COUNT_ASC("count asc, topic"), COUNT_DESC("count desc, topic"), TOPIC_DESC("topic desc, count asc"), TOPIC_ASC("topic asc, count, asc");

    private String sortClause;
    FacetSortOrder(String sortClause) {
        this.sortClause = sortClause;
    }

    public String getSortClause() {
        return sortClause;
    }
}
