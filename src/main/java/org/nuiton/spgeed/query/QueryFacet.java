package org.nuiton.spgeed.query;

public abstract class QueryFacet {

    /** facet name */
    protected String name;
    /**
     * maximum topic to retrieve
     */
    protected int max = 10;
    protected FacetSortOrder sort = FacetSortOrder.COUNT_ASC;

    public QueryFacet(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public FacetSortOrder getSort() {
        return sort;
    }

    public void setSort(FacetSortOrder sort) {
        this.sort = sort;
    }

    /**
     * retourne la requete SQL qui retourne le calcul pour cette facette
     * @param tableSource le nom de la vue dans lequel sont les resultats
     */
    abstract public String getQuery(String tableSource);
}
