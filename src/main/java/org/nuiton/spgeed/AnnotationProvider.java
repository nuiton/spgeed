package org.nuiton.spgeed;

import java.lang.annotation.Annotation;

public interface AnnotationProvider<T extends Annotation> {

     T getAnnotation(Class<T> c);
}
