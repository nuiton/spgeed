package org.nuiton.spgeed;

import org.nuiton.spgeed.query.QueryFacets;

import java.sql.ResultSet;
import java.util.Optional;

public interface SpgeedMapper {

    /**
     *
     * @param sql primitive sql query
     * @param chunk previous chunk used to create next call
     * @param roleContraints contraint on role need by user
     * @return sql converted for this mapper
     */
    String getSql(AnnotationProvider annotationProvider, String sql, Optional<Chunk> chunk, QueryFacets facets, Optional<String> roleContraints);

    /**
     *
     * @param rs ResultSet to transform
     * @param returnType wanted final type
     * @param elementType element type if returnType is collection or array
     * @return new object of returnType Class
     * @throws Exception if error during conversion
     */
    <E> E getResult(AnnotationProvider annotationProvider, ResultSet rs, Class<E> returnType, Class elementType) throws Exception;
}
