package org.nuiton.spgeed.result;

import org.nuiton.spgeed.Facets;

import java.util.LinkedHashSet;
import java.util.Set;

public class SetWithFacet<E> extends LinkedHashSet<E> implements Facets {
    protected FacetResult facetResult;

    @Override
    public FacetResult getFacets() {
        return facetResult;
    }

    @Override
    public void setFacets(FacetResult facetResult) {
        this.facetResult = facetResult;
    }
}
