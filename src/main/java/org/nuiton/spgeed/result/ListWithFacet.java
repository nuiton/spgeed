package org.nuiton.spgeed.result;

import org.nuiton.spgeed.Facets;

import java.util.ArrayList;

public class ListWithFacet<E> extends ArrayList<E> implements Facets {
    protected FacetResult facetResult;

    @Override
    public FacetResult getFacets() {
        return facetResult;
    }

    @Override
    public void setFacets(FacetResult facetResult) {
        this.facetResult = facetResult;
    }
}
