package org.nuiton.spgeed;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.nuiton.spgeed.result.FacetResult;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * Use to paging result.
 *
 * <li>fetch: paging size</li>
 * <li>first: first element position</li>
 * <li>next: use for next call, this is first in next call</li>
 * <li>total: total number of result without paging</li>
 * <li>size: number of result retrieve in this paging (less or equals to fetch)</li>
 *
 * @param <T>
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonIgnoreProperties({"elementType", "empty"})
public class ChunkArrayList<T> extends AbstractList<T> implements Chunk<T>, Facets {
    protected ArrayList<T> result = new ArrayList<>();

    protected Class<T> elementType;
    protected long fetch;
    protected long first;
    protected long next;
    protected long total;
    protected FacetResult facetResult;

    public ChunkArrayList() {}

    @Override
    public int size() {
        return result.size();
    }

    @Override
    public T get(int index) {
        return result.get(index);
    }

    @Override
    public T set(int index, T element) {
        return result.set(index, element);
    }

    @Override
    public boolean add(T t) {
        return result.add(t);
    }

    @Override
    public T remove(int index) {
        return result.remove(index);
    }

    public ChunkArrayList(long fetch, long first, long next, long total) {
        this.fetch = fetch;
        this.first = first;
        this.next = next;
        this.total = total;
    }

    @Override
    public Class<T> getElementType() {
        return elementType;
    }

    @Override
    public Chunk<T> setElementType(Class<T> elementType) {
        this.elementType = elementType;
        return this;
    }

    @Override
    public long getFetch() {
        return fetch;
    }

    @Override
    public Chunk<T> setFetch(long fetch) {
        this.fetch = fetch;
        return this;
    }

    @Override
    public long getFirst() {
        return first;
    }

    @Override
    public Chunk<T> setFirst(long first) {
        this.first = first;
        return this;
    }

    @Override
    public long getNext() {
        return next;
    }

    @Override
    public Chunk<T> setNext(long next) {
        this.next = next;
        return this;
    }

    @Override
    public long getTotal() {
        return total;
    }

    @Override
    public Chunk<T> setTotal(long total) {
        this.total = total;
        return this;
    }

    @Override
    public List<T> getResult() {
        return result;
    }

    @Override
    public FacetResult getFacets() {
        return facetResult;
    }

    @Override
    public void setFacets(FacetResult facetResult) {
        this.facetResult = facetResult;
    }

}
