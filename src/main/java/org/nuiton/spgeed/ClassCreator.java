package org.nuiton.spgeed;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.implementation.InvocationHandlerAdapter;
import net.bytebuddy.matcher.ElementMatchers;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.spgeed.annotations.Script;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;
import org.nuiton.spgeed.query.Query;

import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * Use to generate proxy for handling the SQL statement on each method from the 
 * class contains annotation (select, update or script).
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ClassCreator {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ClassCreator.class);

    public<E> E generate(Class<E> c, SqlSession session, Object... constructorArgs) throws Exception {
        final InvocationHandler handler = createInvocationHandler(session);

        InvocationHandlerAdapter invocationHandlerAdapter = InvocationHandlerAdapter.of(handler);

        ByteBuddy byteBuddy = new ByteBuddy();
        Class<? extends E> loaded = byteBuddy.subclass(c)
            .method(ElementMatchers.named("getSession")).intercept(FixedValue.value(session))
            .method(ElementMatchers.isAnnotatedWith(Update.class)).intercept(invocationHandlerAdapter)
            .method(ElementMatchers.isAnnotatedWith(Select.class)).intercept(invocationHandlerAdapter)
            .method(ElementMatchers.isAnnotatedWith(Script.class)).intercept(invocationHandlerAdapter)
            .make()
            .load(Thread.currentThread().getContextClassLoader())
            .getLoaded();

        E result = ConstructorUtils.invokeConstructor(loaded, constructorArgs);
        return result;
    }

    protected <E> InvocationHandler createInvocationHandler(SqlSession session) {

        /** Manage the limit annotation **/
        BiFunction<String, Long, Void> runTimeLogger = (String annotationLimit, Long startTime) -> {
            if (!log.isWarnEnabled()) {
                return null;
            }

            String limit = ("".equals(annotationLimit)) ? session.getDefaultTimeOutLimit() : annotationLimit;

            if ("".equals(annotationLimit)) {
                return null;
            }

            Long limitMillis = SpgeedUtils.parsePeriod(limit);
            Long queryTime = System.currentTimeMillis() - startTime;
            if (queryTime >= limitMillis) {
                log.warn(String.format("Execution time exceed %s: %s", limit, SpgeedUtils.getTimer(queryTime)));
            }
            return null;
        };

        InvocationHandler handler = (proxy, method, args) -> {
            Long StartTime = System.currentTimeMillis();
            // Map the pararemeters by name
            Map<String, Object> parameterValues = new HashMap<>();
            Parameter[] parameters = method.getParameters();

            int index = 0;
            for (Parameter parameter : parameters) {
                String name = parameter.getName();
                parameterValues.put(name, args[index++]);
            }

            // Return
            Class<?> returnClass = method.getReturnType();
            Type returnType = method.getGenericReturnType();

            if (method.isAnnotationPresent(Update.class)) {
                Update annotation = method.getDeclaredAnnotation(Update.class);
                String sql = annotation.sql();
                Class mapper = annotation.mapper();
                String[] roles = annotation.roles();
                String limitRuntime = annotation.timeout();
                if (mapper == SpgeedMapper.class) {
                    mapper = session.getDefaultMapper();
                }

                Query query = new Query(session, sql, mapper, type -> method.getDeclaredAnnotation(type), roles, parameterValues, returnClass, returnType);
                E result = query.executeUpdate();
                runTimeLogger.apply(limitRuntime, StartTime);
                return result;
            }

            if (method.isAnnotationPresent(Select.class)) {
                Select annotation = method.getDeclaredAnnotation(Select.class);
                String sql = annotation.sql();
                Class mapper = annotation.mapper();
                String[] roles = annotation.roles();
                String limitRuntime = annotation.timeout();
                if (mapper == SpgeedMapper.class) {
                    mapper = session.getDefaultMapper();
                }

                Query query = new Query(session, sql, mapper, type -> method.getDeclaredAnnotation(type), roles, parameterValues, returnClass, returnType);
                E result = query.executeQuery();
                runTimeLogger.apply(limitRuntime, StartTime);
                return result;
            }

            if (method.isAnnotationPresent(Script.class)) {
                Script annotation = method.getDeclaredAnnotation(Script.class);
                String file = annotation.file();
                String limitRuntime = annotation.timeout();

                ClassLoader classLoader = SqlSession.class.getClassLoader();
                InputStream resource = classLoader.getResourceAsStream(file);
                String content = IOUtils.toString(resource, StandardCharsets.UTF_8);

                Query query = new Query(session, content, parameterValues, Boolean.class);
                boolean result = query.execute();
                runTimeLogger.apply(limitRuntime, StartTime);
                return result;
            }
            throw new UnsupportedOperationException("Method not supported " + method.getName());
        };

        return handler;
    }
}
