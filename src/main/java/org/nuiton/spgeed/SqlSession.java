package org.nuiton.spgeed;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import org.nuiton.spgeed.mapper.JsonMapper;
import org.nuiton.spgeed.query.PipeFunction;
import org.nuiton.spgeed.query.PipeFunctions;
import org.nuiton.spgeed.query.Query;
import org.postgresql.ds.PGSimpleDataSource;

/**
 * Represents your connection and the transaction on the database. Be carful, you need to close the 
 * current session, after each use.
 * 
 * @author julien
 */
public class SqlSession implements AutoCloseable {

    /**
     * Current pipe functions available
     */
    protected Map<String, PipeFunction> pipeFunctions;
    
    /**
     * Current datasource
     */
    protected DataSource dataSource;
    
    /**
     * Current JDBC connection
     */
    protected Connection connection;

    /**
     * Use to generate proxy on the dao
     */
    protected ClassCreator classCreator;

    /**
     * Use to set a default limit of time for PostgreSQL request
     */
    protected String defaultTimeOutLimit = "";

    /**
     * Use to set a default Mapper
     */
    protected Class<? extends SpgeedMapper> defaultMapper = JsonMapper.class;

    public SqlSession() {
        this.classCreator = new ClassCreator();
        this.pipeFunctions = new HashMap<>();
        
        PipeFunctions functions = new PipeFunctions();
        this.addFunction(functions.getExposedFunction());
    }
    
    /**
     * Create session from url, username and password
     */
    public SqlSession(String url, String username, String password) {
        this();
        
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setUrl(url);
        dataSource.setUser(username);
        dataSource.setPassword(password);

        this.dataSource = dataSource;
    }

    /**
     * Create session from a datasource
     */
    public SqlSession(DataSource dataSource) {
        this();
        this.dataSource = dataSource;
    }

    /**
     * Get the actual default mapper
     *
     * @return the actual default limit
     */
    public Class<? extends SpgeedMapper> getDefaultMapper() {
        return defaultMapper;
    }

    /**
     * Set a default mapper
     *
     * @param defaultMapper class wanted as default mapper
     */
    public void setDefaultMapper(Class<? extends SpgeedMapper> defaultMapper) {
        this.defaultMapper = defaultMapper;
    }
    /**
     * Get the actual default limit of time for PostgreSQL request
     *
     * @return the actual default limit
     */
    public String getDefaultTimeOutLimit() {
        return defaultTimeOutLimit;
    }

    /**
     * Use to set a default limit of time for PostgreSQL request
     *
     * @param defaultTimeOutLimit time wanted as default limit
     */
    public void setDefaultTimeOutLimit(String defaultTimeOutLimit) {
        this.defaultTimeOutLimit = defaultTimeOutLimit;
    }

    public DataSource getDataSource() {
        return dataSource;
    }
    
    public void addFunction(String name, PipeFunction function) {
        this.pipeFunctions.put(name, function);
    }

    public void addFunction(Map<String, PipeFunction> functions) {
        this.pipeFunctions.putAll(functions);
    }

    public Map<String, PipeFunction> getPipeFunctions() {
        return this.pipeFunctions;
    }
    
    /**
     * Use to get the current transaction when the SQL statements is executed.
     */
    public Connection getConnection() throws SQLException {
        if (this.connection == null || this.connection.isClosed()) {
            this.connection = dataSource.getConnection();
            this.connection.setAutoCommit(false);
        }
        return this.connection;
    }
    
    /**
     * Commit the current transaction.
     */
    public void commit() throws SQLException {
        if (this.connection != null && !this.connection.isClosed()) {
            this.connection.commit();
        }
    }
    
    /**
     * Rollback the current transaction.
     */
    public void rollback() throws SQLException {
        if (this.connection != null && !this.connection.isClosed()) {
            this.connection.rollback();
        }
    }
    
    /**
     * Close the current transaction.
     */
    @Override
    public void close() throws SQLException {
        if (this.connection != null && !this.connection.isClosed()) {
            this.connection.commit();
            this.connection.close();
        }
    }
    
    /**
     * Get a current dao plug on the session.
     *
     * @param klass class of Dao wanted
     * @param args args use to create dao, dao must has constructor with that args
     */
    public<E> E getDao(Class<E> klass, Object... args) {
        try {
            E result = classCreator.generate(klass, this, args);
            return result;
        } catch(Exception eee) {
            throw new RuntimeException("Can't generate proxy class for " + klass, eee);
        }
    }

    public<E> E executeQuery(String sql, Map<String, Object> parameters, Class<E> returnClass, String... roles) throws Exception {
        Query q = new Query(this, sql, parameters, returnClass, roles);
        return q.executeQuery();
    }

    public int executeUpdate(String sql, Map<String, Object> parameters, String... roles) throws Exception {
        Query q = new Query(this, sql, parameters, Integer.class, roles);
        return q.executeQuery();
    }

    public boolean execute(String sql, Map<String, Object> parameters, String... roles) throws Exception {
        Query q = new Query(this, sql, parameters, Integer.class, roles);
        return q.execute();
    }
}
