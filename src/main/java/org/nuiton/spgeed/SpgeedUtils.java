package org.nuiton.spgeed;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpgeedUtils {

    /**
     * Retourne le type qui doit etre retourné, c-a-d que si l'utilisateur
     * demande:
     * - MyObject[] on retourne MyObject
     * - Collection&lt;MyObject&gt; on retourne MyObject
     * - Chunk&lt;MyObject&gt; on retourne MyObject
     *
     * @param c
     * @return
     */
    static private Log log = LogFactory.getLog(SpgeedUtils.class);

    public static Optional<Class> getElementType(Type c) {
        Class result = null;

        if (c instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType)c;
            Type raw = pType.getRawType();
            if (raw instanceof Class && Collection.class.isAssignableFrom((Class)raw)) {
                Type tmp = pType.getActualTypeArguments()[0];
                if (tmp instanceof Class) {
                    result = (Class) tmp;
                }
            }
        } else if (c instanceof Class && ((Class)c).isArray()) {
            result = ((Class)c).getComponentType();
        }
        return Optional.ofNullable(result);
    }

    public static Collection newCollectionInstance(Class returnType) {
        if (!Collection.class.isAssignableFrom(returnType)) {
            throw new IllegalArgumentException("Class is not a collection: " + returnType);
        }

        Collection result;
        if(Modifier.isAbstract( returnType.getModifiers() ) || returnType.isInterface()) {
            if (returnType.isAssignableFrom(Chunk.class)) {
                result = new ChunkArrayList();
            } else if (returnType.isAssignableFrom(Set.class)) {
                result = new HashSet();
            } else if (returnType.isAssignableFrom(List.class) || returnType.isAssignableFrom(Collection.class)) {
                result = new ArrayList();
            } else {
                throw new IllegalArgumentException("Can't find instanciable class for :" + returnType);
            }
        } else {
            try {
                result = (Collection)returnType.newInstance();
            } catch (InstantiationException | IllegalAccessException eee) {
                throw new IllegalArgumentException("Can't create new instance of class :" + returnType);
            }
        }
        return result;
    }

    public static Object convertToNumber(Object v, Class elementType) {
        try {
            if (v == null || elementType.isInstance(v)) {
                return v;
            } else {
                Method m = Number.class.getMethod(elementType.getSimpleName().toLowerCase().replace("integer", "int") + "Value");
                return m.invoke(v);
            }
        } catch (Exception eee) {
            throw new RuntimeException(String.format("Can't convert '%s' to '%s'", v, elementType.getName()), eee);
        }
    }

    public static boolean returningChunk(Class returnType) {
        boolean result = Chunk.class.isAssignableFrom(returnType);
        return result;
    }

    public static boolean returningFacets(Class returnType) {
        boolean result = Facets.class.isAssignableFrom(returnType);
        return result;
    }

    public static boolean returningCollection(Class returnType) {
        boolean result = Collection.class.isAssignableFrom(returnType);
        return result;
    }

    public static boolean returningArray(Class returnType) {
        boolean result = returnType.isArray();
        return result;
    }

    public static boolean returningPrimitive(Class elementType) {
        boolean result = ClassUtils.isPrimitiveOrWrapper(elementType)
                || elementType.equals(String.class)
                || elementType.equals(UUID.class)
                || elementType.isEnum();
        return result;
    }

    public static <T, C extends Chunk<T>> C newChunk(Class<C> returnType, Class<T> elementType, long fetch, long first, long next, long total) {
        try {
            C result = returnType.newInstance();
            result.setElementType(elementType);
            result.setFetch(fetch);
            result.setFirst(first);
            result.setNext(next);
            result.setTotal(total);
            return result;
        } catch (InstantiationException | IllegalAccessException eee) {
            throw new IllegalArgumentException("Chunk child class must have no args constructor", eee);
        }
    }

    /**
     * Convert an amount of time in ms into a String
     *
     * @param nb Amout of time in ms
     * @return nb converted to a String with unit (Hour, Minute, Second, MilliSecond)
     */
    public static String getTimer(long nb) {
        if (nb <= 0) {
            return "0ms";
        }

        long multipliers[] = {
                60 * 60 * 1000, // 1 Hour
                60 * 1000, // 1 Minute
                1000, // 1 Second
                1 // 1 Millisecond
        };
        String units[] = {"h", "m", "s", "ms"};

        String result = "";

        for (int i = 0; i < units.length; i++) {
            long multiplier = multipliers[i];
            String unit = units[i];

            if (nb / multiplier > 0) {
                result += nb / multiplier + unit;
                nb %= multiplier;
                if (nb != 0) {
                    result += " ";
                }
            }
        }
        return result;
    }

    /**
     * Convert and parse the period like "1h 2m 3s 400ms" to ms
     *
     * @param period representing an amount of time with their unit
     * @return the value of period in ms
     */
    public static Long parsePeriod(String period){
        if(period == null) return null;

        boolean isPassed = false;
        final Pattern periodPattern = Pattern.compile("([0-9]+)((ms)|([hdms]))");

        period = period.toLowerCase(Locale.ENGLISH);
        Matcher matcher = periodPattern.matcher(period);
        Instant instant= Instant.EPOCH;
        while (matcher.find()) {
            isPassed = true;
            int num = Integer.parseInt(matcher.group(1));
            String typ = matcher.group(2);
            switch (typ) {
                case "ms":
                    instant = instant.plus(Duration.ofMillis(num));
                    break;
                case "m":
                    instant = instant.plus(Duration.ofMinutes(num));
                     break;
                case "s":
                    instant=instant.plus(Duration.ofSeconds(num));
                    break;
                case "h":
                    instant=instant.plus(Duration.ofHours(num));
                    break;
                case "d":
                    instant=instant.plus(Duration.ofDays(num));
                    break;
            }
        }
        if (isPassed == false) {
            throw new IllegalArgumentException(String.format("Wrong format %s doesn't exist\n", period));
        }
        return instant.toEpochMilli();
    }
}
