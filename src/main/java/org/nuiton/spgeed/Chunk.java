package org.nuiton.spgeed;

import java.util.Collection;

public interface Chunk<T> extends Collection<T>, Facets {
    static Chunk init(long fetch) {
        Chunk result = new ChunkArrayList(fetch, 0, 0, 0);
        return result;
    }

    static Chunk restart(long fetch, long last) {
        Chunk result = new ChunkArrayList(fetch, 0, last, 0);
        return result;
    }

    default Collection<T> getResult() {
        return this;
    }


    default void set(Class<T> elementType, long fetch, long first, long next, long total) {
        setElementType(elementType);
        setFetch(fetch);
        setFirst(first);
        setNext(next);
        setTotal(total);
    }

    Class<T> getElementType();

    Chunk<T> setElementType(Class<T> elementType);

    long getFetch();

    Chunk<T> setFetch(long fetch);

    long getFirst();

    Chunk<T> setFirst(long first);

    long getNext();

    Chunk<T> setNext(long next);

    long getTotal();

    Chunk<T> setTotal(long total);

}
