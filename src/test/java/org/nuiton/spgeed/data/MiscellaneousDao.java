package org.nuiton.spgeed.data;

import org.nuiton.spgeed.annotations.Script;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;
import org.nuiton.spgeed.mapper.simpleflatmapper.SimpleFlatMapper;
import org.nuiton.spgeed.mapper.simpleflatmapper.SimpleFlatMapperConfig;

import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public interface MiscellaneousDao {

    @Script(file = "initMiscellaneous.sql")
    boolean initData();

    @Select(sql= "Select title FROM miscellaneous")
    String[] getTitles();

    @Select(sql= "Select number FROM miscellaneous")
    Double[] getNumbers();

    @Select(sql= "Select * FROM miscellaneous")
    Miscellaneous[] getAll();

    @Update(sql = "CREATE TABLE IF NOT EXISTS miscnote (id UUID primary key, note TEXT, misc UUID)")
    int createMiscNote();

    @Update(sql = "INSERT INTO miscnote (id, note, misc) VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, ${note}, ${miscellaneous})")
    int saveMiscNote(String note, UUID miscellaneous);

    @Select(sql= "Select * FROM miscellaneous m left join miscnote n on m.id = n.misc ", mapper = SimpleFlatMapper.class)
    @SimpleFlatMapperConfig(ids = "id")
    Miscellaneous[] getAllMiscWithNote();
}
