package org.nuiton.spgeed.data;

import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class MiscNote {

    protected UUID id;
    protected String note;
    protected UUID misc;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public UUID getMisc() {
        return misc;
    }

    public void setMisc(UUID misc) {
        this.misc = misc;
    }
}
