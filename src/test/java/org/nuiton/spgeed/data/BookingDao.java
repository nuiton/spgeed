package org.nuiton.spgeed.data;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.json.simple.JSONArray;
import org.nuiton.spgeed.SpgeedDao;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

/**
 *
 * @author ymartel (martel@codelutin.com)
 *
 */
public abstract class BookingDao implements SpgeedDao {

    @Update(sql = "CREATE TABLE IF NOT EXISTS booking (name TEXT, fromDate TIMESTAMP, toDateTime TIMESTAMP, departure TIMESTAMPTZ)")
    abstract public int createBooking();

    @Update(sql = "INSERT INTO booking (name, fromDate, toDateTime, departure) VALUES (${name}, ${fromDate}, ${toDateTime}, ${departure})")
    abstract public int save(String name, LocalDate fromDate, LocalDateTime toDateTime, OffsetDateTime departure);

    @Select(sql = "SELECT * FROM booking")
    abstract public JSONArray getBookingArray();

    @Select(sql = "SELECT * FROM booking")
    abstract public Booking[] getBookings();

    @Select(sql = "SELECT * FROM booking order by fromDate DESC")
    abstract public Booking[] getBookingsOrderByFromDateDesc();

    @Select(sql = "SELECT * FROM booking order by toDateTime DESC")
    abstract public Booking[] getBookingsOrderByToDateDesc();

    @Select(sql = "SELECT * FROM booking order by departure DESC")
    abstract public Booking[] getBookingsOrderByDepartureDesc();

    @Select(sql = "SELECT * FROM booking where name = ${name}")
    abstract public Booking getBookingObject(String name);
}
