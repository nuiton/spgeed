package org.nuiton.spgeed.data;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;

/**
 *
 * @author julien
 */
public interface RoleDao  {

    @Update(sql = "GRANT ALL ON Journey TO manager, employee")
    abstract public int grant();
    
    @Update(sql = "CREATE ROLE manager")
    abstract public int createManagerRole();
    
    @Update(sql = "CREATE ROLE employee")
    abstract public int createEmployeeRole();
    
    @Update(sql = "CREATE USER jack IN ROLE manager")
    abstract public int createManager();
    
    @Update(sql = "CREATE USER john IN ROLE employee")
    abstract public int createEmployee();
    
    @Update(sql = "SET LOCAL SESSION AUTHORIZATION \"jack\"")
    int setManager();
    
    @Update(sql = "SET LOCAL SESSION AUTHORIZATION \"john\"")
    int setEmployee();
    
    @Update(sql = "RESET SESSION AUTHORIZATION")
    int resetUser();
    
    @Select(sql = "SELECT * FROM journey", roles = {"manager"})
    abstract public Journey[] getJourneysWithUserRole();

}
