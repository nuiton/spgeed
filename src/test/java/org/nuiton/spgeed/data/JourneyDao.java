package org.nuiton.spgeed.data;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONArray;
import org.nuiton.spgeed.query.Query;
import org.nuiton.spgeed.SpgeedDao;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;

/**
 *
 * @author julien
 */
public abstract class JourneyDao implements SpgeedDao {
    
    @Update(sql = "CREATE TABLE IF NOT EXISTS journey (name TEXT, location TEXT)")
    abstract public int createJourney();
    
    @Update(sql = "INSERT INTO journey (name, location) VALUES (${name}, ${location})")
    abstract public int save(String name, String location);

    @Select(sql = "SELECT * FROM journey")
    abstract public JSONArray getJourneyArray();
    
    @Select(sql = "SELECT * FROM journey")
    abstract public Journey[] getJourneys();
    
    @Select(sql = "SELECT * FROM journey where name LIKE ${name}")
    abstract public Journey getJourneyObject(String name);

    @Select(sql = "SELECT * FROM journey where name = ${journey.name}")
    abstract public Journey[] getJourneyObject(Journey journey);

    @Select(sql = "SELECT * FROM journey where name = ${journey.name | toParam()}")
    abstract public Journey[] getJourneyPipe(Journey journey);
    
    @Select(sql = "SELECT count(*) FROM journey", timeout = "1ms")
    abstract public int getJourneyCount();
    
    public Journey[] getJourneyDelegate(Journey journey) {
        return this.getJourneyObject(journey);
    }

    public Journey[] getJourneyManual(Journey journey) throws Exception {
        Map map = new HashMap();
        map.put("journey", journey);
        Query query = new Query(getSession(), "SELECT * FROM journey where name = ${journey.name}", map, Journey[].class);
        return (Journey[])query.executeQuery();
    }
    
    @Select(sql = "UPDATE journey SET name = ${newName} WHERE name = ${oldName} RETURNING location")
    abstract public String updateJourneyName(String oldName, String newName);

    @Select(sql = "SELECT name FROM journey")
    abstract public String[] getJourneyNames();

    @Select(sql = "SELECT count(name) FROM journey GROUP BY location")
    abstract public Integer[] countJourneysByLocationAsInteger();

    @Select(sql = "SELECT count(name) FROM journey GROUP BY location")
    abstract public int[] countJourneysByLocationAsInt();
}
