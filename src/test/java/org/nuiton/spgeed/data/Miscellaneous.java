package org.nuiton.spgeed.data;

import java.util.List;
import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Miscellaneous {

    protected UUID id;
    protected String title;
    protected Double number;
    protected List<MiscNote> miscNote;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }

    public List<MiscNote> getMiscNote() {
        return miscNote;
    }

    public void setMiscNote(List<MiscNote> miscNote) {
        this.miscNote = miscNote;
    }
}
