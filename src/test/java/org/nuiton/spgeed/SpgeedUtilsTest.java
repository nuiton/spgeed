package org.nuiton.spgeed;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.SingleInstancePostgresRule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.spgeed.data.*;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test parsePeriod && getTimer
 * 
 * @author killian
 */
public class SpgeedUtilsTest {

    final long millis = 1;
    final long second = 1000 * millis;
    final long minute = 60 * second;
    final long hour = 60 * minute;

    @Test
    public void converterMillisToLong() {

        long result = SpgeedUtils.parsePeriod("1ms");

        Assert.assertEquals(1 * millis, result);
    }

    @Test
    public void converterSecondToLong() {

        long result = SpgeedUtils.parsePeriod("1s");

        Assert.assertEquals(1 * second, result);
    }

    @Test
    public void converterMinuteToLong() {

        long result = SpgeedUtils.parsePeriod("1m");

        Assert.assertEquals(1 * minute, result);
    }

    @Test
    public void converterHourToLong() {

        long result = SpgeedUtils.parsePeriod("1h");

        Assert.assertEquals(1 * hour, result);
    }

    @Test
    public void converterTimeWithMultipleUnit() {

        long result = SpgeedUtils.parsePeriod("4m2s40ms1h");

        Assert.assertEquals(1 * hour + 4 * minute + 2 * second + 40 * millis, result);
    }

    @Test
    public void convertTimeWithMultipleSameUnit() {

        long result = SpgeedUtils.parsePeriod("40ms20ms");

        Assert.assertEquals(40 * millis + 20 * millis, result);
    }

    @Test
    public void convertTimeWithSpaces() {

        long result = SpgeedUtils.parsePeriod("40ms 20ms");

        Assert.assertEquals(40 * millis + 20 * millis, result);
    }

    @Test
    public void invalidConvertTimeFormat() {

        try {
            long result = SpgeedUtils.parsePeriod("40az");
            Assert.assertTrue(false);
        } catch (Exception E) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void convertMillisToTime() {

        Assert.assertEquals("10ms", SpgeedUtils.getTimer(10 * millis));
    }

    @Test
    public void convertSecondToTime() {

        Assert.assertEquals("1s", SpgeedUtils.getTimer(second));
    }

    @Test
    public void convertMinuteToTime() {

        Assert.assertEquals("10m", SpgeedUtils.getTimer(10 * minute));
    }

    @Test
    public void convertHourToTime() {

        Assert.assertEquals("1h", SpgeedUtils.getTimer(hour));
    }

    @Test
    public void convertLongToTime() {

        Assert.assertEquals("2h 40m 50s 400ms", SpgeedUtils.getTimer(2 * hour + 40 * minute + 50 * second + 400 * millis));
    }

    @Test
    public void getTimerNegativeValueOrEqual0() {

        Assert.assertEquals("0ms", SpgeedUtils.getTimer(0));
        Assert.assertEquals("0ms", SpgeedUtils.getTimer(-1));
    }
}
