package org.nuiton.spgeed.query;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

import org.nuiton.spgeed.SqlSession;
import org.postgresql.util.PGobject;
        
/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class QueryParserTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(QueryParserTest.class);

    class MyClass {
        public Object toto;
        public Object titi;

        public MyClass(Object toto, Object titi) {
            this.toto = toto;
            this.titi = titi;
        }
    }
    
    protected Query parse(String q) throws Exception {
        SqlSession session = new SqlSession();
        Map<String, Object> params = new HashMap<>();
        params.put("name", Arrays.asList(
                new MyClass(new MyClass("first", "FIRST"), null),
                new MyClass(new MyClass("second", "SECOND"), null)
        ));
        
        params.put("encore", "toto.toto");
        params.put("values", Arrays.asList(1, 2, 3));
        params.put("anObject", new MyClass("tai", "st"));
        Query query = new Query(session, q, params, Object[].class);
        query.getParsedSql(); // Force running the parsing
        return query;
    }
    
    @Test
    public void nothing() throws Exception {
        Query query = parse("Salut tout le monde");
        assertThat(query.parsedSql).isEqualTo("Salut tout le monde");
        assertThat(query.sqlParameters).isEmpty();
    }
    
    @Test
    public void properties() throws Exception {
        Query query = parse("Hello tout ${ name[1].toto.titi } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout ? le monde");
        assertThat(query.sqlParameters).containsExactly("SECOND");
    }
    
    @Test
    public void string() throws Exception {
        Query query = parse("Hello tout ${ encore | toString() } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout toto.toto le monde");
        assertThat(query.sqlParameters).isEmpty();
    }
    
    @Test
    public void toParam() throws Exception {
        Query query = parse("Hello tout ${ encore | toParam() } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout ? le monde");
        assertThat(query.sqlParameters).containsExactly("toto.toto");
    }
    
    @Test
    public void json() throws Exception {
        Query query = parse("Hello tout ${ encore | json() } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout ? le monde");
        assertThat(query.sqlParameters).extracting("value").containsExactly("\"toto.toto\"");
        
        query = parse("Hello tout ${ values | json() } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout ? le monde");
        assertThat(query.sqlParameters).extracting("value").containsExactly("[1,2,3]");
        
        query = parse("Hello tout ${ anObject | json() } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout ? le monde");
        assertThat(query.sqlParameters).extracting("value").containsExactly("{\"toto\":\"tai\",\"titi\":\"st\"}");
    }
    
    @Test
    public void array() throws Exception {
        Query query = parse("Hello tout ${ encore | array() } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout ARRAY[?] le monde");
        assertThat(query.sqlParameters).containsExactly("toto.toto");
        
        query = parse("Hello tout ${ values | array() } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout ARRAY[?, ?, ?] le monde");
        assertThat(query.sqlParameters).containsExactly(1, 2, 3);
    }

    @Test
    public void collection() throws Exception {
        Query query = parse("Hello tout ${ encore | collection() } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout (?) le monde");
        assertThat(query.sqlParameters).containsExactly("toto.toto");

        query = parse("Hello tout ${ values | collection() } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout (?, ?, ?) le monde");
        assertThat(query.sqlParameters).containsExactly(1, 2, 3);
    }
    
    @Test
    public void values() throws Exception {
        Query query = parse("Hello tout ${ anObject | values('toto', 'titi') } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout (?, ?) le monde");
        assertThat(query.sqlParameters).containsExactly("tai", "st");
        
        query = parse("Hello tout ${ name | values('toto', 'titi') } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout (?, ?), (?, ?) le monde");
        assertThat(query.sqlParameters).extracting(json -> json != null ? ((PGobject) json).getValue() : null).containsExactly(
                "{\"toto\":\"first\",\"titi\":\"FIRST\"}",
                null, 
                "{\"toto\":\"second\",\"titi\":\"SECOND\"}",
                null);
    }
    
    @Test
    public void map() throws Exception {
        Query query = parse("Hello tout ${ anObject | map('toto') } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout [tai] le monde");
        assertThat(query.sqlParameters).isEmpty();
    }
    
    @Test
    public void row() throws Exception {
        Query query = parse("Hello tout ${ anObject | row('toto', 'titi') } le monde");
        assertThat(query.parsedSql).isEqualTo("Hello tout ROW(?, ?) le monde");
        assertThat(query.sqlParameters).containsExactly("tai", "st");
    }
    
    @Test
    public void complex() throws Exception {
        Query query = parse("Hello tout ${ name | map(${encore}) | array() | toString() } le monde ${ name[1].toto.toto};");
        assertThat(query.parsedSql).isEqualTo("Hello tout ARRAY[?, ?] le monde ?;");
        assertThat(query.sqlParameters).containsExactly("first", "second", "second");
    }
}
