package org.nuiton.spgeed;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.nuiton.spgeed.data.Journey;
import org.nuiton.spgeed.data.JourneyDao;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.SingleInstancePostgresRule;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.sql.DataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.spgeed.data.MiscNote;
import org.nuiton.spgeed.data.Miscellaneous;
import org.nuiton.spgeed.data.MiscellaneousDao;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test basic requests
 * 
 * @author julien
 */
public class SqlTest {
    
    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(SqlTest.class);
    
    protected JourneyDao journeyDao;
    protected MiscellaneousDao miscellaneousDao;

    @Rule
    public SingleInstancePostgresRule database = EmbeddedPostgresRules.singleInstance();
    
    @Before
    public void initData() throws SQLException {
        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        try (SqlSession session = new SqlSession(ds)) {
            this.journeyDao = session.getDao(JourneyDao.class);
            this.miscellaneousDao = session.getDao(MiscellaneousDao.class);
            
            journeyDao.createJourney();
            journeyDao.save("Guadeloupe", "Océan Atlantique");
            journeyDao.save("Martinique", "Océan Atlantique");
            journeyDao.save("Guyane", "Océan Atlantique");
            journeyDao.save("La Réunion", "Océan Indien");
            journeyDao.save("Mayotte", "Océan Indien");
        }
    }

    @Test 
    public void selectAsJsonArray() {
        JSONArray journeyArray = journeyDao.getJourneyArray();
        log.info("journeyArray " + journeyArray);
        assertThat(journeyArray).isNotNull();
    }

    @Test 
    public void selectAsPrimitif() {
        int count = journeyDao.getJourneyCount();
        log.info("count " + count);
        assertThat(count).isEqualTo(5);
    }

    @Test 
    public void selectAsObject() {
        Journey journeyObject = journeyDao.getJourneyObject("%adeloupe");
        log.info("journeyObject " + journeyObject);
        assertThat(journeyObject).isNotNull();
    }

    @Test 
    public void selectWithObjectParam() {
        Journey journey = new Journey();
        journey.setName("Martinique");
        Journey[] journeyObject = journeyDao.getJourneyObject(journey);
        log.info("journeyObject " + Arrays.toString(journeyObject));
        assertThat(journeyObject).isNotNull();
    }

    @Test 
    public void selectWithPipe() {
        Journey journey = new Journey();
        journey.setName("Guyane");
        Journey[] journeyObject = journeyDao.getJourneyPipe(journey);
        log.info("journeyPipe " + Arrays.toString(journeyObject));
        assertThat(journeyObject).isNotNull();
    }

    @Test 
    public void selectDelegate() {
        Journey journey = new Journey();
        journey.setName("La Réunion");
        Journey[] journeyObject = journeyDao.getJourneyDelegate(journey);
        log.info("journeyDelegate " + Arrays.toString(journeyObject));
        assertThat(journeyObject).isNotNull();
    }

    @Test 
    public void selectManual() throws Exception {
        Journey journey = new Journey();
        journey.setName("Mayotte");
        Journey[] journeyObject = journeyDao.getJourneyManual(journey);
        log.info("journeyManual " + Arrays.toString(journeyObject));
        assertThat(journeyObject).isNotNull();
    }
    
    @Test
    public void updateReturning() throws Exception {
        String location = journeyDao.updateJourneyName("Guadeloupe", "Guadeloup");
        log.info("updateReturning " + location);
        assertThat(location).isEqualTo("Océan Atlantique");
    }

    @Test
    public void selectStringArray() throws Exception {
        String[] names = journeyDao.getJourneyNames();
        log.info("journey names " + Arrays.toString(names));
        assertThat(names).isNotNull();
        assertThat(names.length).isEqualTo(5);
    }

    @Test
    public void selectIntegerArray() throws Exception {
        Integer[] names = journeyDao.countJourneysByLocationAsInteger();
        log.info("journeyManual " + Arrays.toString(names));
        assertThat(names).isNotNull();
        assertThat(names.length).isEqualTo(2);
    }

    @Test
    public void selectIntArray() throws Exception {
        int[] names = journeyDao.countJourneysByLocationAsInt();
        log.info("journeyManual " + Arrays.toString(names));
        assertThat(names).isNotNull();
        assertThat(names.length).isEqualTo(2);
    }

    @Test
    public void selectDoubleArrayWithIntValues() throws Exception {
        miscellaneousDao.initData();
        Double[] numbers = miscellaneousDao.getNumbers();
        log.info("numbers " + Arrays.toString(numbers));
        assertThat(numbers).isNotNull();
        assertThat(numbers.length).isEqualTo(4);
        List<Double> doubleList = Arrays.asList(numbers);
        Assert.assertTrue(doubleList.contains(3.14159265359d));
        Assert.assertTrue(doubleList.contains(1.61803398875d));
        Assert.assertTrue(doubleList.contains(2d));
        Assert.assertTrue(doubleList.contains(null));
    }

    @Test
    public void selectFlat() throws Exception {
        miscellaneousDao.initData();
        miscellaneousDao.createMiscNote();

        Miscellaneous[] all = miscellaneousDao.getAll();
        miscellaneousDao.saveMiscNote("note1", all[0].getId());
        miscellaneousDao.saveMiscNote("note2", all[0].getId());
        Miscellaneous[] allWithNotes = miscellaneousDao.getAllMiscWithNote();
        Assert.assertEquals(2, allWithNotes[0].getMiscNote().size());
        Assert.assertEquals(Stream.of("note1", "note2").collect(Collectors.toSet()), allWithNotes[0].getMiscNote().stream().map(MiscNote::getNote).collect(Collectors.toSet()));
    }
}
