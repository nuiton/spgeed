package org.nuiton.spgeed;

/*-
 * #%L
 * spgeed
 * %%
 * Copyright (C) 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.spgeed.data.Journey;
import org.nuiton.spgeed.data.JourneyDao;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.SingleInstancePostgresRule;
import java.sql.SQLException;
import java.util.Arrays;
import javax.sql.DataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.nuiton.spgeed.data.ScriptDao;

/**
 * Test on role
 * 
 * @author julien
 */
public class ScriptTest {
    
    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ScriptTest.class);
    
    protected JourneyDao journeyDao;
    protected ScriptDao scriptDao;
    
    @Rule
    public SingleInstancePostgresRule database = EmbeddedPostgresRules.singleInstance();
    
    @Before
    public void initData() throws SQLException {
        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        try (SqlSession session = new SqlSession(ds)) {
            this.journeyDao = session.getDao(JourneyDao.class);
            this.scriptDao = session.getDao(ScriptDao.class);
            
            journeyDao.createJourney();
            
            journeyDao.save("Guadeloupe", "Océan Atlantique");
            journeyDao.save("Martinique", "Océan Atlantique");
            journeyDao.save("Guyane", "Océan Atlantique");
            journeyDao.save("La Réunion", "Océan Indien");
            journeyDao.save("Mayotte", "Océan Indien");
        }
    }
    
    @Test
    public void execScript() throws Exception {
        this.scriptDao.updateData();
        
        Journey[] journeys = journeyDao.getJourneys();
        log.info("names " + Arrays.toString(journeys));
        assertThat(journeys).extracting("name").contains("GUADELOUPE", "MARTINIQUE", "GUYANE", "LA RÉUNION", "MAYOTTE");
    }
    
}
