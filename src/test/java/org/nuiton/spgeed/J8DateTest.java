package org.nuiton.spgeed;

import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.SingleInstancePostgresRule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.spgeed.data.Booking;
import org.nuiton.spgeed.data.BookingDao;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class J8DateTest {

    private static final String HO_CHI_MINH = "Ho Chi Minh";
    private static final String CORSICA = "Corsica";
    private static final String COLOMBO = "Colombo";
    protected static final LocalDate CORSICA_FROM_DATE = LocalDate.of(2016, 8, 17);
    protected static final LocalDateTime CORSICA_TO_DATE_TIME = LocalDateTime.of(2016, 8, 31, 23, 24, 55);
    protected static final OffsetDateTime CORSICA_DEPARTURE = OffsetDateTime.of(2016, 8, 31, 17, 2, 55, 0, ZoneOffset.UTC);
    protected static final LocalDate HCM_FROM_DATE = LocalDate.of(2016, 12, 02);
    protected static final LocalDateTime HCM_TO_DATE_TIME = LocalDateTime.of(2016, 12, 15, 8, 24, 0);
    protected static final OffsetDateTime HCM_DEPARTURE = OffsetDateTime.of(2016, 12, 14, 22, 2, 0, 0, ZoneOffset.UTC);
    static private Log log = LogFactory.getLog(SqlTest.class);

    protected BookingDao bookingDao;

    @Rule
    public SingleInstancePostgresRule database = EmbeddedPostgresRules.singleInstance();

    @Before
    public void initData() throws SQLException {
        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        try (SqlSession session = new SqlSession(ds)) {
            this.bookingDao = session.getDao(BookingDao.class);

            bookingDao.createBooking();
            bookingDao.save(CORSICA, CORSICA_FROM_DATE, CORSICA_TO_DATE_TIME, CORSICA_DEPARTURE);
            bookingDao.save(HO_CHI_MINH, HCM_FROM_DATE, HCM_TO_DATE_TIME, HCM_DEPARTURE);
            bookingDao.save(COLOMBO, LocalDate.of(2015, 9, 6),
                            LocalDateTime.of(2015, 9, 29, 22, 24, 0),
                            OffsetDateTime.of(2015, 9, 30, 5, 2, 0, 0, ZoneOffset.UTC));
        }
    }

    @Test
    public void testSelect() {
        Booking booking = bookingDao.getBookingObject("Corsica");
        assertThat(booking).isNotNull();
        assertThat(booking.getFromDate()).isEqualTo(LocalDate.of(2016, 8, 17).toString());
        assertThat(booking.getToDateTime()).isEqualTo(LocalDateTime.of(2016, 8, 31, 23, 24, 55).toString());
        assertThat(booking.getDeparture()).isEqualTo(OffsetDateTime.of(2016, 8, 31, 17, 2, 55, 0, ZoneOffset.UTC));
    }

    @Test
    public void testSelectAsJsonArray() {
        JSONArray bookingArray = bookingDao.getBookingArray();
        log.info("bookingArray " + bookingArray);
        assertThat(bookingArray).isNotNull();
    }

    @Test
    public void testSelectArray() {
        Booking[] bookings = bookingDao.getBookings();
        assertThat(bookings).isNotNull();
        assertThat(bookings.length).isEqualTo(3);
        assertThat(bookings[1].getName()).isEqualTo(HO_CHI_MINH);
        assertThat(bookings[1].getFromDate()).isEqualTo(HCM_FROM_DATE);
        assertThat(bookings[1].getToDateTime()).isEqualTo(HCM_TO_DATE_TIME);
        assertThat(bookings[1].getDeparture()).isEqualTo(HCM_DEPARTURE);
        assertThat(bookings[0].getToDateTime()).isEqualTo(CORSICA_TO_DATE_TIME);
    }

    @Test
    public void testSelectOrderByDate() {
        Booking[] bookings = bookingDao.getBookingsOrderByFromDateDesc();
        assertThat(bookings).isNotNull();
        assertThat(bookings.length).isEqualTo(3);
        assertThat(bookings[0].getName()).isEqualTo(HO_CHI_MINH);
        assertThat(bookings[1].getName()).isEqualTo(CORSICA);
        assertThat(bookings[2].getName()).isEqualTo(COLOMBO);
    }

    @Test
    public void testSelectOrderByDateTime() {
        Booking[] bookings = bookingDao.getBookingsOrderByToDateDesc();
        assertThat(bookings).isNotNull();
        assertThat(bookings.length).isEqualTo(3);
        assertThat(bookings[0].getName()).isEqualTo(HO_CHI_MINH);
        assertThat(bookings[1].getName()).isEqualTo(CORSICA);
        assertThat(bookings[2].getName()).isEqualTo(COLOMBO);
    }

    @Test
    public void testSelectOrderByOffsetDateTime() {
        Booking[] bookings = bookingDao.getBookingsOrderByDepartureDesc();
        assertThat(bookings).isNotNull();
        assertThat(bookings.length).isEqualTo(3);
        assertThat(bookings[0].getName()).isEqualTo(HO_CHI_MINH);
        assertThat(bookings[1].getName()).isEqualTo(CORSICA);
        assertThat(bookings[2].getName()).isEqualTo(COLOMBO);
    }

}
