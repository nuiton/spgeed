
CREATE TABLE IF NOT EXISTS miscellaneous(
    id              UUID    PRIMARY KEY,
    title           TEXT,
    number          double precision
);

-- add some data
INSERT INTO miscellaneous VALUES (md5(random()::text || clock_timestamp()::text)::uuid, 'Pi', 3.14159265359);
INSERT INTO miscellaneous VALUES (md5(random()::text || clock_timestamp()::text)::uuid, 'Golden Ratio', 1.61803398875);
INSERT INTO miscellaneous VALUES (md5(random()::text || clock_timestamp()::text)::uuid, 'Premier premier', 2); -- here a potential error : pure int value will be get in JSON as '2' not as '2.0'
INSERT INTO miscellaneous VALUES (md5(random()::text || clock_timestamp()::text)::uuid, 'No number', null); -- here a potential error : null value
