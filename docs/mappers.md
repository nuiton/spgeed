Spgeed implements mappers to convert the result sets returned by PostgreSQL into POJOs : JsonMapper and SimpleFlatMapper.

Spgeed have a default timeout that can be set with SqlSession :
```java
SqlSession session = /* Your session */;

session.setDefault("Your timeout limit");
```
## JsonMapper

JsonMapper is the default mapper used by Spgeed.
This implementation relies on PostgreSQL JSON capability to convert result sets into JSON format. From there, either the result can be used "as-is" and returned directly to the clients (if you are developping a REST api for instance) or the result is converted and injected into POJOs thanks to Jackson library.

You can create your own mapper that inherits of JsonMapper.
```java
public class YourMapper extends JsonMapper {
    @Override
    public void configureObjectMapper(ObjectMapper mapper) {
        super.configureObjectMapper(mapper);
        SimpleModule module = new SimpleModule("CustomModel", Version.unknownVersion());
        /* Your config */
        mapper.registerModule(module);
    }
}
```

## SimpleFlatMapper

SimpleFlatMapper is another mapper implementation based on [SimpleFlatMapper](http://simpleflatmapper.org/) library.

SimpleFlatMapper has been introduced to make `JOIN` statements more naturals.

Example:
with JsonMapper, here is how you would declare a query to retrieve a list of journeys and the corresponding hotels :
```java
@Select(sql = "SELECT j.name, json_agg(h.*) as hotels "
            + "FROM journey j "
            + "JOIN hotel h ON h.journeyId = j.id "
            + "GROUP BY j.name;")
Journey[] getAll();
```
Here is how you would do it with SimpleFlatMapper :

```java
@Select(sql = "SELECT * FROM journey j JOIN hotel h ON h.journeyId = j.id",
        mapper = SimpleFlatMapper.class)
@SimpleFlatMapperConfig(ids = "id")
Journey[] getAll();
```

In the second case, you don't need to bother about `json_agg` when writing statements.

However you need to an extra annotation `@SimpleFlatMapperConfig` to indicate to `SimpleFlatMapper` which are the column names correponding to each entity id in the database. This information is used by the mapper to identify entities in the result set.

In the previous example, both the journey table and the hotel table use a column named "id" to store journeys and hotels ids : `@SimpleFlatMapperConfig(ids = "id")`
