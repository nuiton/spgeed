Spgeed have an optional parameter "timeout".

If the query timer exceed the amount of time set,
Spgeed will log the query time and the timeout limit at the end of the request.

#### Format
The variable timeout need a String that work as follow :

* "1ms" =   1 millisecond
* "1s"  =   1 second
* "1m"  =   1 minute
* "1h"  =   1 hour

```java
@Select(sql = "select * from journey", timeout = "1s 2m25h")
```
The default limit is set to one second.

The default timeout limit can be changed with SqlSession :
```java
SqlSession session = /* Your session */;

session.setDefaultTimeOutLimit("Your timeout limit");
```